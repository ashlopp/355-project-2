/**
 * Created by Ashlop on 5/17/17.
 */
var mysql  = require('mysql');
var db  = require('./db_connection.js');



/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM customer;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};
//
//
//
exports.getById = function(customer_id, callback) {
    var query = 'SELECT * FROM customer WHERE customer_id = ?';
    var queryData = [customer_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//
//
//
exports.insert = function(params, callback) {
    var query = 'INSERT INTO customer(first_name, last_name, phone_number) VALUES (?, ?, ?)';
    var queryData = [params.first_name, params.last_name, params.phone_number];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//
//
//
exports.delete = function(customer_id, callback) {
    var query = 'DELETE FROM customer WHERE customer_id = ?';
    var queryData = [customer_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};