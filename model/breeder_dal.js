/**
 * Created by Ashlop on 5/18/17.
 */
var mysql  = require('mysql');
var db  = require('./db_connection.js');



/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM breeder;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};
//
//
//
exports.getById = function(breeder_id, callback) {
    var query = 'SELECT * FROM breeder WHERE breeder_id = ?';
    var queryData = [breeder_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//
//
//
exports.insert = function(params, callback) {
    var query = 'INSERT INTO breeder(breeder_name, breed, num_animals) VALUES (?, ?, ?)';
    var queryData = [params.breeder_name, params.breed, params.num_animals];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//
//
//
exports.delete = function(breeder_id, callback) {
    var query = 'DELETE FROM breeder WHERE breeder_id = ?';
    var queryData = [breeder_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};