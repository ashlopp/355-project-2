/**
 * Created by Ashlop on 5/18/17.
 */
var mysql  = require('mysql');
var db  = require('./db_connection.js');



/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM shelter;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};
//
//
//
exports.getById = function(shelter_id, callback) {
    var query = 'SELECT * FROM shelter WHERE shelter_id = ?';
    var queryData = [shelter_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//
//
//
exports.insert = function(params, callback) {
    var query = 'INSERT INTO shelter(shelter_name, num_animals) VALUES (?, ?)';
    var queryData = [params.shelter_name, params.num_animals];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//
//
//
exports.delete = function(shelter_id, callback) {
    var query = 'DELETE FROM shelter WHERE shelter_id = ?';
    var queryData = [shelter_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};