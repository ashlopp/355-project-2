/**
 * Created by Ashlop on 5/18/17.
 */
/**
 * Created by Ashlop on 5/17/17.
 */
var mysql  = require('mysql');
var db  = require('./db_connection.js');



/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM animal;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};
//
//
//
exports.getById = function(animal_id, callback) {
    var query = 'SELECT * FROM animal WHERE animal_id = ?';
    var queryData = [animal_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//
//
//
exports.insert = function(params, callback) {
    var query = 'INSERT INTO animal(name, breed, age, gender) VALUES (?, ?, ?, ?)';
    var queryData = [params.name, params.breed, params.age, params.gender];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//
//
//
exports.delete = function(animal_id, callback) {
    var query = 'DELETE FROM animal WHERE animal_id = ?';
    var queryData = [animal_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};