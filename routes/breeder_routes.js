/**
 * Created by Ashlop on 5/18/17.
 */
var express = require('express');
var router = express.Router();
var breeder_dal = require('../model/breeder_dal');


// View All breeder
router.get('/all', function(req, res) {
    breeder_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('breeder/breederViewAll', {'result':result});
        }
    });

});
//
//
// View the breeder for the given id
router.get('/', function(req, res){
    if(req.query.breeder_id == null) {
        res.send('breeder_id is null');
    }
    else {
        breeder_dal.getById(req.query.breeder_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('breeder/breederViewById', {'result': result});
            }
        });
    }
});
//
//
//
router.get('/edit', function(req, res){
    if(req.query.breedr_id == null) {
        res.send('A breeder ID is required');
    }
    else {
        breeder_dal.edit(req.query.breeder_id, function(err, result){
            res.render('breeder/breederUpdate', {breeder: result});
        });
    }

});
//
//
// Return the add a new breeder form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    breeder_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('breeder/breederAdd', {'result': result});
        }
    });
});

// View the breeder for the given id
router.get('/insert', function(req, res){
    // simple validation
    //might need to be a different variable to match table?
    if(req.query.breeder_name == '') {
        res.send('Please enter a name!');
    }
    if(req.query.num_animals == ''){
        res.send('Please enter number of animals!');
    }
    if(req.query.breed == ''){
        res.send('Please enter a breed');
    }
        // passing all the query parameters (req.query) to the insert function instead of each individually
        breeder_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/breeder/all');
            }
        });
    });
//
//
// Delete a breeder for the given breeder_id
router.get('/delete', function(req, res){
    if(req.query.breeder_id == null) {
        res.send('breeder_id is null');
    }
    else {
        breeder_dal.delete(req.query.breeder_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/breeder/all');
            }
        });
    }
});
//
//
//
router.get('/edit', function(req, res){
    if(req.query.breeder_id == null) {
        res.send('A breeder id is required');
    }
    else {
        breeder_dal.getById(req.query.breeder_id, function(err, breeder) {
            res.render('breeder/breederUpdate', {breeder: breeder});
        });
    }
});
//
//
//
router.get('/update', function(req, res){
  breeder_dal.update(req.query, function(err, result){
        res.redirect(302, '/breeder/all');
    });
});

module.exports = router;