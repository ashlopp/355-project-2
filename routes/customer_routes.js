/**
 * Created by Ashlop on 5/17/17.
 */
var express = require('express');
var router = express.Router();
var customer_dal = require('../model/customer_dal');


// View All customers
router.get('/all', function(req, res) {
    customer_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('customer/customerViewAll', {'result':result});
        }
    });

});
//
//
// View the customer for the given id
router.get('/', function(req, res){
    if(req.query.customer_id == null) {
        res.send('customer_id is null');
    }
    else {
        customer_dal.getById(req.query.customer_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('customer/customerViewById', {'result': result});
            }
        });
    }
});
//
//
//
router.get('/edit', function(req, res){
    if(req.query.customer_id == null) {
        res.send('A Customer ID is required');
    }
    else {
        customer_dal.edit(req.query.customer_id, function(err, result){
            res.render('customer/customerUpdate', {customer: result});
        });
    }

});
//
//
// Return the add a new customer form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    customer_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('customer/customerAdd', {'result': result});
        }
    });
});

// View the customer for the given id
router.get('/insert', function(req, res){
    // simple validation
    //might need to be a different variable to match table?
    if(req.query.first_name == '') {
        res.send('Please enter a first name!');
    }
    if(req.query.last_name == ''){
        res.send('Please enter a Last name!');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        customer_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/customer/all');
            }
        });
    }
});
//
//
// Delete a customer for the given customer_id
router.get('/delete', function(req, res){
    if(req.query.customer_id == null) {
        res.send('customer_id is null');
    }
    else {
        customer_dal.delete(req.query.customer_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/customer/all');
            }
        });
    }
});
//
//
//
router.get('/edit', function(req, res){
    if(req.query.customer_id == null) {
        res.send('A customer id is required');
    }
    else {
        customer_dal.getById(req.query.customer_id, function(err, customer) {
            res.render('customer/customerUpdate', {customer: customer});
        });
    }
});
//
//
//
router.get('/update', function(req, res){
    customer_dal.update(req.query, function(err, result){
        res.redirect(302, '/customer/all');
    });
});

module.exports = router;