/**
 * Created by Ashlop on 5/18/17.
 */
var express = require('express');
var router = express.Router();
var animal_dal = require('../model/animal_dal');


// View All animal
router.get('/all', function(req, res) {
    animal_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('animal/animalViewAll', {'result':result});
        }
    });

});
//
//
// View the animal for the given id
router.get('/', function(req, res){
    if(req.query.animal_id == null) {
        res.send('animal_id is null');
    }
    else {
       animal_dal.getById(req.query.animal_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('animal/animalViewById', {'result': result});
            }
        });
    }
});
//
//
//
router.get('/edit', function(req, res){
    if(req.query.animal_id == null) {
        res.send('A animal ID is required');
    }
    else {
        animal_dal.edit(req.query.animal_id, function(err, result){
            res.render('animal/animalUpdate', {animal: result});
        });
    }

});
//
//
// Return the add a new animal form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    animal_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('animal/animalAdd', {'result': result});
        }
    });
});

// View the animal for the given id
router.get('/insert', function(req, res){
    // simple validation
    //might need to be a different variable to match table?
    if(req.query.name == '') {
        res.send('Please enter a name!');
    }
    if(req.query.age == ''){
        res.send('Please enter age!');
    }
    if(req.query.gender == ''){
        res.send('Please enter a gender');
    }
    if(req.query.breed == ''){
        res.send('Please enter a Breed');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        animal_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/animal/all');
            }
        });
    }
});
//
//
// Delete a animal for the given animal_id
router.get('/delete', function(req, res){
    if(req.query.animal_id == null) {
        res.send('animal_id is null');
    }
    else {
        animal_dal.delete(req.query.animal_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/animal/all');
            }
        });
    }
});
//
//
//
router.get('/edit', function(req, res){
    if(req.query.animal_id == null) {
        res.send('A animal id is required');
    }
    else {
        animal_dal.getById(req.query.animal_id, function(err, animal) {
            res.render('animal/animalUpdate', {animal: animal});
        });
    }
});
//
//
//
router.get('/update', function(req, res){
    animal_dal.update(req.query, function(err, result){
        res.redirect(302, '/animal/all');
    });
});

module.exports = router;